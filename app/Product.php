<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
     use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'description', 'image','category_id','status','deleted_at','ritter_part_number','vender_part_number','price'];
	 public function Category(){
		return $this->hasMany('App\Category','id');
	}
    public function categoryName(){
        return $this->hasMany('App\Category','id','category_id')->select('*','id as category_id');
    }
    public function productImage(){
        return $this->hasMany('App\Image','product_id','id')->select('*','id as product_image_id');
    }
}
