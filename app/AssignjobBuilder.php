<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignjobBuilder extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'assign_job_to_builder';
    protected $fillable = [
        'id', 'job_id','job_builder_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    
}
