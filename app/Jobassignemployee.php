<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Jobassignemployee extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobassignemployee';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_id', 'job_employee_id'];

    // public function jobList(){
    //         return $this->belongTo
    //         ('App\Jobassignemployee','job_employee_id')->join('job','job.id','=','jobassignemployee.job_id')->select('');
    // }
    public function jobList(){
        return $this->hasMany('App\Job','id','job_id')->where('status','active')->select('*');
    }
     public function JobtaskList(){
        return $this->hasMany('App\Jobtask','job_id','job_id')->select('*','jobtask.id as jobtask_id');
    } 
    
    public function jobfolderList(){
        return $this->hasMany('App\Folder','job_id','job_id')->select('*','folder.id as folder_id');
    }
    public function checkemployeeJob(){
        return $this->hasMany('App\Employeejob','job_id','job_id')->select('*','employeejob.id as checkemployee_relational_id');;
    }

     public function jobtaskfolderList(){
        return $this->hasMany('App\Taskfolder','job_id','job_id')->select('*','taskfolder.id as jobtask_folder_id');
    }
    

    
}
