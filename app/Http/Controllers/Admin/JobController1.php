<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Job;
use App\Folder;
use App\Jobtask;
use App\Folderdocument;
use App\Client;
use App\Jobassignemployee;
use Session;
use App\User;
use App\Jobcard;
use App\Builder;
use App\AssignjobBuilder;
use App\Foldernotes;
use DB;
use App\Taskfolder;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
       
        return view('admin.job.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
        $client = Client::pluck('name','id')->prepend('Select Client',''); 
        $builder = Builder::pluck('name','id')->prepend('Select Builder',''); 
        $user = User::select('users.*',  'roles.name as role_name', 'roles.id as role_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', ['EMP'])
            ->where('users.status','=','active')->get();
		return view('admin.job.create',compact('client','user','builder'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'job_number'=>'required',
            'title' => 'required',
            'description' => 'required',
            'client_id'=>'required',
            'job_employee_id'  => 'required|array|min:1', 
            'duedate' => 'required',
            'notes' => 'required', 
            'job_builder_id'  => 'required', 
        ];

        $customMessages = [
            'client_id.required' => 'Please Select Client',
            'job_employee_id.required' => 'Please Select Employee',
            'job_builder_id.required' => 'Please Select Builder',
        ];

        $this->validate($request, $rules, $customMessages);
		$data = $request->all();
       

        if(!empty($request->input('job_employee_id'))){
             if(!empty($request->input('job_builder_id'))){
                 $job = Job::create($data);
                 $job->created_by=$job->id;
                 $job->status='active';
                 $job->job_status='pending';
                 $job->save();
                  $assignjobbuilder = new AssignjobBuilder();
                        $assignjobbuilder->job_id = $job->id;
                        $assignjobbuilder->job_builder_id = $request->input('job_builder_id');
                        $assignjobbuilder->save();
                foreach($request->input('job_employee_id') as $job_employee_id){

                        $jobassignemp = new Jobassignemployee();
                        $jobassignemp->job_id = $job->id;
                        $jobassignemp->job_employee_id = $job_employee_id;
                        $jobassignemp->save();
                }
             }
        }

        Session::flash('flash_message', 'Job added!');

        return redirect('admin/job/'.$job->id.'/edit');
    }

    public function datatable(request $request)
    {
        $job = Job::select('*')->orderBy('id','desc'); 

        if ($request->has('filter_status') && $request->get('filter_status') != '' && $request->get('filter_status') != 'all') {
            $job->where('job.status', $request->get('filter_status'), 'OR');
        }
         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(title LIKE  '%$value%')";

                $job=Job::whereRaw($where_filter);
            }
        }     

        return Datatables::of($job)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   
        $job = Job::findOrFail($id);
        //change job status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $job->status= 'inactive';
                $job->update();            

                return redirect()->back();
            }else{
                $job->status= 'active';
                $job->update();               
                return redirect()->back();
            }

        }

        return view('admin.job.show', compact('job','client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        
        $request->id=$id;
		$job = Job::where('id',$id)->first();
        $client = Client::pluck('name','id');
        $folder = Folder::with('documentList','videolist')->where('job_id',$id)->orderby('sortno','asc')->get();
        $taskfolder = Taskfolder::with('taskList')->where('job_id',$id)->orderby('sortno','asc')->get();
        $notes=Foldernotes::with('noteslist')->where('job_id',$id)->orderby('id','desc')->get();
       //dd($taskfolder);
        $jobtask = Jobtask::where('job_id',$id)->orderby('id','desc')->get();
        $user = User::select('users.*',  'roles.name as role_name', 'roles.id as role_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', ['EMP'])
            ->where('users.status','=','active')->get();
        $jobassignemp = Jobassignemployee::where('job_id',$id)->get();
        $jobcarddetail = Jobcard::with('jobcarduserName')->where('job_id',$id)->select('*')->orderBy('id','desc');
        $builder = Builder::pluck('name','id');
        $assignjobbuilder = AssignjobBuilder::where('job_id',$id)->first();
        $job_builder_id=$assignjobbuilder->job_builder_id;

        return view('admin.job.edit', compact('job','client','folder','taskfolder','notes','jobtask','user','jobassignemp','jobcarddetail','job_builder_id','builder'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $rules = [
            'job_number'=>'required',
            'title' => 'required',
            'description' => 'required',
            'client_id'=>'required',
            'job_employee_id'  => 'required|array|min:1', 
            'duedate' => 'required',
            'notes' => 'required', 
            'job_builder_id'  => 'required',
        ];

        $customMessages = [
            'client_id.required' => 'Please Select Client',
            'job_employee_id.required' => 'Please Select Employee',
            'job_builder_id.required' => 'Please Select Builder',
        ];

        $this->validate($request, $rules, $customMessages);
        $requestData = $request->all();              
        $job = Job::findOrFail($id);
	    $job->update($requestData);
        $jobassignemployee = Jobassignemployee::where('job_id',$id);
        $jobassignemployee->delete();
        
        foreach($request->input('job_employee_id') as $job_employee_id){

                        $jobassignemp = new Jobassignemployee();
                        $jobassignemp->job_id = $job->id;
                        $jobassignemp->job_employee_id = $job_employee_id;
                        $jobassignemp->save();
                }
        $assignjobbuilder = AssignjobBuilder::where('job_id',$id);
        $assignjobbuilder->delete();
                        $assignjobbuilder = new AssignjobBuilder();
                        $assignjobbuilder->job_id = $job->id;
                        $assignjobbuilder->job_builder_id = $request->input('job_builder_id');
                        $assignjobbuilder->save();
        flash('Job Updated Successfully!');
		
        return redirect('admin/job');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
       
        $job = Job::find($id);
        $job->delete();

          $message='Deleted';
        return response()->json(['message'=>$message],200);
    }  

    public function jobcardlist(Request $request,$id)
    {   
        $jobcarddetail = Jobcard::with('jobcarddetailList','jobcarduserName')->where('id',$id)->get();
        return view('admin.job.jobcard',compact('jobcarddetail'));
    }

}
