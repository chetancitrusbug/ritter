<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use App\Foldervideo;
class VideoController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
       
        return view('admin.videos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
        
        return view('admin.videos.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
     {
		 $rules = [
        'title' => 'required',
        'description' => 'required',
        'video_url' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
    ];

    $customMessages = [
         'video_url.required' => 'Please Upload Video'
    ];

    $this->validate($request, $rules, $customMessages);
		$data = $request->all();

         if ($request->file('video_url')) {

            $video_url = $request->file('video_url');
            $filename = uniqid(time()) . '.' . $video_url->getClientOriginalExtension();

            $video_url->move(public_path('Video'), $filename);

            $data['video_url'] = $filename;

        }
        $data['user_type']='admin';
        $video = Video::create($data);

        Session::flash('flash_message', 'Video added!');

        return redirect('admin/videos');
    }

     public function datatable(request $request)
    {
        $video = Video::where('user_type','admin')->orderBy('id','desc')->select('*'); ;        

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(title LIKE  '%$value%')";
                $video=Video::whereRaw($where_filter);
            }
        }     
        return Datatables::of($video)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $video = Video::find($id);
        
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $video->status= 'inactive';
                $video->update();            

                return redirect()->back();
            }else{
                $video->status= 'active';
                $video->update();               
                return redirect()->back();
            }

        }
		if($video){
            return view('admin.videos.show', compact('video'));
        }
        else{
             return redirect('/admin/videos');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$video = Video::where('id',$id)->first();

        return view('admin.videos.edit', compact('video'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
		$rules = [
        'title' => 'required',
        'description' => 'required',
         'video_url' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
    ];

    $customMessages = [
         'video_url.required' => 'Please Upload Video',
    ];

    $this->validate($request, $rules, $customMessages);
        $requestData = $request->all();              
        $video = Video::findOrFail($id);

        if ($request->file('video_url')) {

            $video_url = $request->file('video_url');
            $filename = uniqid(time()) . '.' . $video_url->getClientOriginalExtension();

            $video_url->move(public_path('Video'), $filename);
        }


		if(isset($filename)){
			 $requestData['video_url'] = $filename;
		}

	    $video->update($requestData);

        flash('Video Updated Successfully!');
		
        return redirect('admin/videos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
       
        $video = Video::find($id);
        $foldervideo = Foldervideo::where('video_id',$id)->get();
        foreach($foldervideo as $delvideo){
            $delvideo->delete();
        }
        $video->delete();
        $message='Deleted';
        return response()->json(['message'=>$message],200);
    }  

}
