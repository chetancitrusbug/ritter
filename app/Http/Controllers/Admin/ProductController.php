<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use App\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        $category = Category::where('status','active')->pluck('name','id')->prepend('Select Category','');
		//dd($category);
		return view('admin.products.create',compact('category'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'category_id' => 'required' ,
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'vender_part_number'=>'required',
            'ritter_part_number'=>'required',
            'price'=>'required',
            'status'=>'required'
        ]);
		$data = $request->all();
        $files = $request->file('image');
        if ($request->file('image')) {
            $i=1;

                foreach ($files as $image) {

                    $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('Products'), $filename);

                    if ($i == 1)
                        {

                            $data['image'] = url('Products').'/'.$filename;
                                $product = Product::create($data);

                        }

                        $image = Image::create($data);
                        $image['image'] = url('Products').'/'.$filename;
                        $image->product_id=$product->id;
                        $image->save();

                         $i++;
                }

        }

        return redirect('admin/products');
    }

public function datatable(request $request)
    {
        $product = Product::join('category', 'products.category_id', '=', 'category.id')
            ->select(['products.*','category.name as cat_name'])
            ->orderBy('products.id','desc');

        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(products.name LIKE '%$value%')";
                $product = Product::join('category', 'products.category_id', '=', 'category.id')
                ->select(['products.*','category.name as cat_name'])->whereRaw($where_filter);
            }
        }

        return Datatables::of($product)
            ->make(true);
        exit;
    }

 /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $product = Product::findOrFail($id);


        return view('admin.products.show', compact('product'));
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
		$product = Product::where('id',$id)->first();
        $images = Image::where('product_id',$id)->get();
        $category = Category::pluck('name','id');
        return view('admin.products.edit', compact('product','category','images'));
    }


 /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[

            'name' => 'required',
            'description' => 'required',
            'category_id' => 'required',
			'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'vender_part_number'=>'required',
            'ritter_part_number'=>'required',
            'status'=>'required',
            'price'=>'required',
        ]);
        $requestData = $request->all();
        $product = Product::findOrFail($id);

         $files = $request->file('image');
        if ($request->file('image')) {
            $i=1;
                if(count($files)){
                    $images = Image::where('product_id',$id);
                    $images->delete();
                foreach ($files as $image) {

                    $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('Products'), $filename);

                    if ($i == 1)
                        {

							$requestData['image'] = url('Products').'/'.$filename;
                            $product->update($requestData);

                        }

                        $image = Image::create($requestData);
						$image['image'] = url('Products').'/'.$filename;
                        $image->product_id=$product->id;
                        $image->save();

                         $i++;
                }
                }


        }

	    $product->update($requestData);

        flash('Document Updated Successfully!');

        return redirect('admin/products');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

    $product = Product::find($id);

    $imageurl = explode('/',$product->image);
    $imgname = array_slice($imageurl, 6); // take everything from offset=6 on
    $path = implode('/',$imgname);
    if($product->image){
        unlink($path);
        }
        $product->delete();

        $images = Image::where('product_id',$id);
        if($images){
            $images->delete();
            $message='Deleted';
        }

        return response()->json(['message'=>$message],200);
    }




    }


