<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tool;
use App\ToolLogs;
use Mail;
use Yajra\Datatables\Datatables;
use DB;
use Session;

class ToolsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.tools.index');
    }

    public function logsView($id, Request $request)
    {
        $tool = Tool::find($id);
        if($tool){
            return view('admin.tools.logs', compact('tool'));
        }
        else{
            Session::flash('flash_message','Tool not Exist!');
             return redirect('/admin/tools');
        }
    }

    public function logsDatatable(request $request,$id)
    {
        $toolLog = Tool::select('*','users.name as user_name')
        ->join('tools_logs','tools_logs.tool_id','tools.id')
        ->join('users','users.id','tools_logs.user_id')
        ->where('tools_logs.tool_id',$id);

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(users.name LIKE  '%$value%')";

                $toolLog= $toolLog->whereRaw($where_filter);
            }
        }
        $toolLog = $toolLog->orderBy('tools_logs.id','desc')->get();

        return Datatables::of($toolLog)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {


		return view('admin.tools.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'tool_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status'=>'required'
        ]);
        $data = $request->all();
        $file = $request->file('tool_image');
        if ($request->file('tool_image')) {
            $filename = uniqid(time()) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('Tools'), $filename);
            $data['tool_image'] = url('Tools').'/'.$filename;
        }
        else {
            $data['tool_image'] = '';
        }
        Session::flash('flash_message','Tool Added!');
        $tool = Tool::create($data);
        return redirect('admin/tools');
    }

public function datatable(request $request)
    {
        $tool = Tool::all();

        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(tools.name LIKE '%$value%')";
                $tool = Tool::whereRaw($where_filter);
            }
        }

        return Datatables::of($tool)
            ->make(true);
        exit;
    }

 /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $tool = Tool::find($id);

        //change client status


        if($tool){
            $status = $request->get('status');
            if (!empty($status)) {
                if ($status == 'active') {
                    $tool->status = 'inactive';
                    $tool->update();

                    return redirect()->back();
                } else {
                    $tool->status = 'active';
                    $tool->update();
                    return redirect()->back();
                }

            }
            return view('admin.tools.show', compact('tool'));
        }
        else{
            Session::flash('flash_message','Tool not Exist!');
            return redirect('admin/tools');
        }

    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $tool = Tool::where('id',$id)->first();
        if($tool != null)
        {
            return view('admin.tools.edit', compact('tool'));
        }
        else{
            Session::flash('flash_message','Tool not Exist!');
        }

    }


 /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[

            'name' => 'required',
            'tool_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
            'status'=>'required'
        ]);
        $requestData = $request->all();
        $tool = Tool::findOrFail($id);
        $file = $request->file('tool_image');
        if ($request->file('tool_image')) {
            $filename = uniqid(time()) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('Tools'), $filename);
            $requestData['tool_image'] = url('Tools').'/'.$filename;
        }
        else {
            $requestData['tool_image'] = '';
        }

	    $tool->update($requestData);

        flash('Document Updated Successfully!');

        return redirect('admin/tools');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

    $tool = Tool::find($id);
    if($tool != null)
    {
        $tool->delete();
    }
    else{
        Session::flash('flash_message','Tool not Exist!');
    }
        return response()->json(['message'=>$message],200);
    }

    public function checkInOutTools(Request $request){


        $rules = array(
            'tool_id'=>'required|integer',
            'in_out_flag'=>'required|in:0,1',
            'date_time'=> 'required|date|date_format:Y-m-d H:i:s|before_or_equal:'.date('Y-m-d H:i:s'),
        );


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            return response()->json(['message' => $message,'code' => 400], 200);

        } else {

            $user_id = \Auth::user()->id;
            $tool_id = $request->tool_id;
            $date_time = $request->date_time;
            $in_out_flag = $request->in_out_flag;

            $tool = Tool::where('id',$tool_id)->first();


                if ($request->in_out_flag == 0) {
                    $toolLogs = ToolLogs::where('tool_id', $tool_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                    if ($toolLogs != null) {

                        $message = 'Already Check In';
                        return response()->json(['message' => $message, 'code' => 400], 200);

                    }else {
                        $tool->flag_check_in_out = 0;
                        $tool->save();
                        // $message = 'Tool Not Available.';
                        // return response()->json(['message' => $message, 'code' => 400], 200);
                        $toolLogs = new toolLogs;
                        $toolLogs->tool_id = $tool_id;
                        $toolLogs->user_id = $user_id;
                        $toolLogs->start_datetime = $date_time;

                        $toolLogs->save();
                        return response()->json(['message' => 'Check In Successfully', 'code' => 200], 200);
                    }
                }
                else {
                    $toolLogs = ToolLogs::where('tool_id', $tool_id)->where('user_id', $user_id)->first();
                    if($toolLogs != null)
                    {
                        $toolLogs = ToolLogs::where('tool_id', $tool_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                        if($toolLogs != null)
                        {
                            $tool->flag_check_in_out = 1;
                            $tool->save();


                            $toolLogs->end_datetime = $date_time;
                            $toolLogs->save();
                            return response()->json(['message' => 'Check Out Successfully', 'code' => 200], 200);

                        }
                        else {
                            $message = 'Already Check Out';
                            return response()->json(['message' => $message, 'code' => 400], 200);

                        }
                    }
                    else {
                        $message = 'No Tools Log Found';
                        return response()->json(['message' => $message, 'code' => 400], 200);

                    }
                }

        };

    }




    }


