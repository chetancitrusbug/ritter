<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tool;
use App\User;
use App\ToolLogs;
use Mail;


class ToolsController extends Controller
{
    public function toollist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $tooldata = Tool::select('tools.id as tool_id','tools.id as id','tools.tool_image as tool_image','tools.name as name','tools.image as image','tools.description as description','tools.category_id as category_id','tools.deleted_at as deleted_at','tools.created_by as created_by','tools.created_at as created_at','tools.updated_at as updated_at','tools.status as status','tools.ritter_part_number as ritter_part_number','tools.vender_part_number as vender_part_number')
            ->get();
            if(count($tooldata)>0){
                         $data[]=$tooldata;
                         $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No tools Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function tooldetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $tool_id = $request->tool_id;
        $users= User::where('users.id',$id)->first();

        if($users){
             $tooldata = Tool::with('categoryName','toolImage')
             ->where('id',$tool_id)
             ->select('tools.id as tool_id','tools.id as id','tools.tool_image as tool_image','tools.name as name','tools.image as image','tools.description as description','tools.category_id as category_id','tools.deleted_at as deleted_at','tools.created_by as created_by','tools.created_at as created_at','tools.updated_at as updated_at','tools.status as status','tools.ritter_part_number as ritter_part_number','tools.vender_part_number as vender_part_number')
            ->get();
            if(count($tooldata)>0){
                 $data=$tooldata;
                 $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No tools Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function toolsAvailableList(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $tooldata = Tool::
            where('tools.flag_check_in_out','=', 1)
            ->select('tools.id as tool_id','tools.*')
            ->where('tools.status','active')
            ->get();

            if(count($tooldata)>0){
                $data=$tooldata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No tools Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function checkInOutTools(Request $request){
        $data = [];
        $message = "Tools logs Saved";
        $status = true;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'tool_id'=>'required|integer',
            'in_out_flag'=>'required|integer',
            'date_time'=> 'required|date'//|date_format:Y-m-d H:i:s|before_or_equal:'.date('Y-m-d H:i:s'),
        );


        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {

            $user_id = $request->user_id;
            $tool_id = $request->tool_id;
            $date_time = $request->date_time;
            $in_out_flag = $request->in_out_flag;

            $users = User::where('users.id',$user_id)->first();
            $tool = Tool::where('id',$tool_id)->first();


            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }

            if(!$tool){
                $status = false;
                $message = 'No Tool Found';
                $code = 400;
            }

            if($status){
                if ($request->in_out_flag == 0) {
                    $toolLogs = ToolLogs::where('tool_id', $tool_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                    if ($toolLogs != null) {
                        $status = false;
                        $message = 'Already Check In';
                        $code = 400;
                    }else {
                        $tool->flag_check_in_out = 0;
                        $tool->save();
                        $toolLogs = new ToolLogs;
                        $toolLogs->tool_id = $tool_id;
                        $toolLogs->user_id = $user_id;
                        $toolLogs->start_datetime = $date_time;
                        $toolLogs->save();
                        $data = $toolLogs;
                    }
                }
                else {
                    $toolLogs = ToolLogs::where('tool_id', $tool_id)->where('user_id', $user_id)->first();
                    if($toolLogs != null)
                    {
                        $toolLogs = ToolLogs::where('tool_id', $tool_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                        if($toolLogs != null)
                        {
                            $tool->flag_check_in_out = 1;
                            $tool->save();

                            $toolLogs->end_datetime = $date_time;
                            $toolLogs->save();
                            $data = $toolLogs;
                        }
                        else {
                            $status = false;
                            $message = 'Already Check Out';
                            $code = 400;
                        }
                    }
                    else {
                        $status = false;
                        $message = 'No Tools Log Found';
                        $code = 400;
                    }
                }
            }
        }
        return response()->json(['data'=>(object)$data, 'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function toolsUsedList(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $tooldata = Tool::
            join('tools_logs','tools_logs.tool_id','tools.id')
            ->where('tools_logs.start_datetime','!=',null)
            ->where('tools_logs.end_datetime','=',null)
            ->where('tools_logs.user_id','=',$id)
            ->where('tools.status','active')
            ->select('tools.id as tool_id','tools.*')
            ->get();
            if(count($tooldata)>0){
                $data=$tooldata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No tools Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }
}

