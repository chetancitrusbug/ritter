<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use App\Productinquiry;
use Mail;


class ProductsController extends Controller
{
    public function productlist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $productdata = Product::with('categoryName','productImage')
             ->select('products.id as product_id','products.id as id','products.name as name','products.image as image','products.description as description','products.category_id as category_id','products.deleted_at as deleted_at','products.created_by as created_by','products.created_at as created_at','products.updated_at as updated_at','products.status as status','products.ritter_part_number as ritter_part_number','products.vender_part_number as vender_part_number','products.price')
            ->get();
            if(count($productdata)>0){
                         $data[]=$productdata;
                         $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Products Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function productdetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $product_id = $request->product_id;
        $users= User::where('users.id',$id)->first();

        if($users){
             $productdata = Product::with('categoryName','productImage')
             ->where('id',$product_id)
             ->select('products.id as product_id','products.id as id','products.name as name','products.image as image','products.description as description','products.category_id as category_id','products.deleted_at as deleted_at','products.created_by as created_by','products.created_at as created_at','products.updated_at as updated_at','products.status as status','products.ritter_part_number as ritter_part_number','products.vender_part_number as vender_part_number','products.price')
            ->get();
            if(count($productdata)>0){
                 $data=$productdata;
                 $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Products Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

     public function productinquiry(Request $request){
        $data = [];
        $message = "Successfully Done";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $product_id = $request->product_id;
        $name = $request->name;
        $emailid = $request->emailid;
        $description = $request->description;
        $quantity = $request->quantity;

        $users= User::where('users.id',$user_id)->first();

        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if($product_id==''){
            $status = false;
            $message = 'Enter Product Id';
            $code = 400;
        }
        if($name==''){
            $status = false;
            $message = 'Enter Name';
            $code = 400;
        }
        if($emailid==''){
            $status = false;
            $message = 'Enter EmailId';
            $code = 400;
        }
        if($quantity==''){
            $status = false;
            $message = 'Enter Quantity';
            $code = 400;
        }
        else if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$emailid))
        {
            $message = 'Invalid Mail';
            $status = false;
            $code = 400;
        }

       $productlist = Product::find($product_id);
        if($status){
            if($users){
                if($productlist){
                    $productdata= new Productinquiry;
                    $productdata->user_id=$user_id;
                    $productdata->product_id=$product_id;
                    $productdata->name=$name;
                    $productdata->emailid=$emailid;
                    $productdata->description=$description;
                    $productdata->quantity=$quantity;
                    $productdata->save();

                    $admin = User::where('users.id', '1')->first();

                    Mail::send('admin.emails.inquerymail', compact('productdata','admin','users','productlist'), function ($message) use ( $productdata ,$admin,$users,$productlist) {
                        $message->to($admin->email)->subject('Product Inquiry');
                    });

                }else{
                    $status = false;
                    $message = 'No Product Found';
                    $code = 400;
                }
            }
            else{
                    $status = false;
                    $message = 'No User Found';
                    $code = 400;
                }
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
}

