<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
class UsersController extends Controller
{

    public function login(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Login Success';
        $status = 'true';
        $error = '';
        $email = $request->email;
        $password = $request->password;
        $device_token = $request->device_token;
        $device_type = $request->device_type;
        $user = User::where('email',$email)->where('deleted_at',null)->first();
        if($user){
            if($user->status != 'active'){
                $messages = 'User is not Active';
                $code = 400;
                $status = 'false';
            }elseif(Hash::check($password, $user->password)){
                $user->api_token = md5(uniqid());
                $user->device_token = $device_token;
                $user->device_type = $device_type;
                $user->update();
                $data = $user;
            }else{
                $messages = 'Password is wrong';
                $code = 400;
                $status = 'false';
            }
        }else{
            $messages = 'Email/User not found';
            $code = 400;
            $status = 'false';
        }
        return response()->json(['data'=>$data,'code'=>$code,'message'=>$messages,'status'=>$status]);
        exit;
    }

    public function forgot(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Forgot passwrod mail Success';
        $status = 'true';
        $error = '';
        $email = $request->email;
        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());

        });
        if ($response == "passwords.sent") {
            $messages = 'Password reset link has been sent to email';
        } else if ($response == "passwords.user") {
            $status = false;
            $messages = 'User not found';
            $code = 400;
        } else {
            $status = false;
            $messages = 'Something went wrong ! Please try again later';
            $code = 400;
        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function register(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Registration Success';
        $status = 'true';
        $error = '';
        $rules = array(
            'name' => 'required',
            'email' => 'required',//|email|unique:users',
            'password' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
            $message = 'Not valid data';
			return $this->toJson($status, $data, $message, $validation, $code);
        } else {
            $simpleUser = User::where('email', $request->input('email'))->first();
            if($simpleUser == null)
            {
                $user = User::where('email', $request->input('email'))->withTrashed()->first();
                $data = $request->except('password');

                if($simpleUser == null && $user == null)
                {
                    $data['password'] = bcrypt($request->password);
                    $data['api_token'] = sha1(time() . uniqid() . $data['email']);
                    $user = User::create($data);
                    $user->assignRole('EMP');
                }
                else if($user != null)
                {
                    $user->restore();
                    $user->update($data);
                }

                if ($user) {
                    $data = $user;
                    $messages = 'Registration Successful.';
                } else {
                    $status = false;
                    $code = \Config::get('constants.responce_code.bad_request');
                    $messages = 'Something went wrong ! Please try again later';
                }
            }
            else {
                $status = false;
                $code = \Config::get('constants.responce_code.bad_request');
                $messages = 'Email ALready exist!';

            }
        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function changePassword(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $validation = [];
        $code = 200;

        $id = $request->user_id;
        $rules = array(
			'old_password'=>'required',
            'password' => 'required|min:6|max:255',
            'password_confirmation' => 'required|same:password',
        );


        $messsages = array(
            'old_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        );

        $validator = \Validator::make($request->all(), $rules, $messsages);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
        } else {

            $user = User::where("id", $id)->first();
		    if ($user && Hash::check($request->input('old_password'), $user->password)) {
				if ($user) {
					$user->password = Hash::make($request->input('password'));
					$user->save();
					$message = 'Password changed successfully.';
				} else {
					$validator->errors()->add('current_password', 'Please enter correct current password');
					$validation = $validator;
					$code = \Config::get('constants.responce_code.validation_failed');
					$status = false;
				}
			}
			else{
				$message = 'Current password is Wrong';
				$status = false;
				$code=400;
			}

        }

		return response()->json(['status'=> $status,'data' => $data, 'message' => $message, 'validation' => $validation,'code'=>$code]);
        exit;
    }


    public function update(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Profile Update';
        $status = true;
        $id = $request->user_id;
		$name = $request->name;
        $user = User::find($id);
        if($name==''){
            $status = false;
            $messages = 'Enter Name';
            $code = '404';
        }
		if($status){
			if($user){
				$user->name = $request->name;
				$user->save();
				$data = $user;
			}else{
				$status = false;
				$messages = 'Profile Can not update';
				$code = '404';
			}
		}
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function logout(Request $request)
    {
        $data = [];
        $message = "";
        $validation = [];
        $code = 200;
        $status = true;

        if ($request->has('api_token')) {
            $api_token = $request->get('api_token');
            User::where("api_token", $api_token)->update(["api_token" => ""]);
        }
        $message = "Logout success!";
        return response()->json(['code' => $code, 'message' => $message, 'status' => $status]);
        exit;
    }

}
