<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToolLogs extends Model
{
    protected $table = 'tools_logs';
    protected $fillable = [
        'tool_id', 'user_id','start_datetime','end_datetime'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
