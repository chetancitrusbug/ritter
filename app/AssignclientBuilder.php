<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignclientBuilder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'assign_client_to_builder';
    protected $fillable = [
        'id', 'client_id','client_builder_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    public function builderName(){
        return $this->belongsTo('App\Builder','client_builder_id','id');
    }
}
