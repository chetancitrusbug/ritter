<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'status_dropdown' => [
        'new' => 'Nouveau',
        'open' => 'Ouvrir',
        'pending' => 'en attendant',
        'on-hold' => 'En attente',
        'solved' => 'Résolu',
        'closed' => 'Fermé',
    ],
    'label'=>[
        'dashboard' => 'Tableau de bord',
        'all_tickets' => 'Tous les billets',
    ],
];
