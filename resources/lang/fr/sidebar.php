<?php

return [

	'customer' => 'Client',
	'my_profile' => 'Mon profil',
	'suppliers' => 'Fournisseurs',
	'banks' => 'Les banques',
	'transaction' => 'Transaction',
	'discount_rate' => "Taux d'escompte",
	'invoice' => 'Invoices'

];