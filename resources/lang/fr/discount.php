<?php

return[

	'discount_rate' => "Taux d'escompte",
	'add_new' => 'Ajouter de nouvelles',
	'id' => 'ID',
	'class' => 'Class',
	'minimum' => 'Au moins',
	'maximum' => 'Maximum',
	'actions' => 'Actions',
	'view' => 'Voir',
	'edit' => 'Modifier',
	'back' => 'Router',
	'edit_discount_rate' => "Taux d'Escompte Modifier",
	'create_new_discount_rate' => "Créer de nouveaux taux d'escompte"

];