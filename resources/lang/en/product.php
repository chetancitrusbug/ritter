<?php

return [

	'add_product' => 'Add Product',
    'add_new_product' => 'Add New Product',
    'back' => 'Back',
    'edit_product' => 'Edit Product',
    'update' => 'Update',
    'title' => 'Title',
	'products' => 'Products',
    'id' => 'Id',
	'status' => 'Status',
	'actions' => 'Actions',
	'active' => 'Active',
    'inactive' => 'Inactive',
    'blocked' => 'Blocked',
	'view' => 'View',
	'product' => 'Product',
	'create' => 'Create',
	'name' => 'Name',
	'description' => 'Description',
	'category'=> 'Product Category',
	'image'=>'Image',
	'vender_part_number'=>'Vender Part Number',
    'ritter_part_number'=>'Ritters Part Number',
    'price'=>'Ritter Price'
];