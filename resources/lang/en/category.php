<?php

return [

	'add_category' => 'Add Category',
    'add_new_category' => 'Add New Category',
    'back' => 'Back',
    'edit_category' => 'Edit Category',
    'update' => 'Update',
    'name' => 'Name',
	'categories' => 'Categories',
    'id' => 'Id',
	'status' => 'Status',
	'actions' => 'Actions',
	'active' => 'Active',
    'inactive' => 'Inactive',
    'blocked' => 'Blocked',
	'view' => 'View',
	'create' => 'Create',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'view_category'=>'View Category',
	'category'=>'Category',
];