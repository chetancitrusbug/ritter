@extends('layouts.backend')

@section('title',trans('tool.edit_tool'))
@section('pageTitle',trans('tool.edit_tool'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('tool.edit_tool')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('document.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($tool, [
                        'method' => 'PATCH',
                        'url' => ['/admin/tools', $tool->id],
                        'class' => 'form-horizontal',
                        'id'=>'formtool',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.tools.form', ['submitButtonText' => trans('document.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
