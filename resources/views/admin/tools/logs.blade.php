@extends('layouts.backend')
@section('title',trans('tool.tools'))
@section('pageTitle',trans('tool.tools'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('tool.tools') @lang('tool.logs')
                    </div>
                </div>

                <div class="box-content ">
                    <div class="row">
                        <a href="{{ url('/admin/tools') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('tool.back')
                            </button>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="tool-table">
                            <thead>
                            <tr>
                                <th data-priority="3">@lang('tool.user_name')</th>
                                <th data-priority="7">@lang('tool.start_datetime')</th>
                                <th data-priority="7">@lang('tool.end_datetime')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
      var dataurl ="{{ url('/admin/tools/logs/datatable') }}";
        datatable = $('#tool-table').DataTable({
           // "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: dataurl+'/'+"{{$tool->id}}",
                    type: "get", // method , by default get

                },
                columns: [
                    { data: 'user_name',name:'user_name',"searchable" : true},
                    { data: 'start_datetime',name:'start_datetime',"searchable" : true},
                    { data: 'end_datetime',name:'end_datetime',"searchable" : true}

                ]
        });


</script>
@endpush
