@extends('layouts.backend')

@section('title',trans('tool.tools'))
@section('pageTitle',trans('tool.tools'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('tool.tools')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/tools/create') }}" class="btn btn-success btn-sm"
                                   title="Add New tool">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('tool.add_new_tool')
                                </a>

                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/tools', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}

                            {!! Form::close() !!}
                            </div>
                        </div>



                    <div class="table-responsive">
                        <table class="table table-borderless" id="tools-table">
                            <thead>
                            <tr>
                                <!--<th data-priority="1">@lang('tool.id')</th>   -->
                                <th data-priority="3">@lang('tool.name')</th>
                                <th data-priority="3">@lang('tool.image')</th>
                                <th data-priority="5">@lang('tool.description')</th>
                                <th data-priority="7">@lang('tool.status')</th>
                                <th data-priority="8">@lang('tool.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="checkInOutToolsView" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Check In Out Tools</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('tool_id', null, ['class' => 'form-control','id' => 'tool_id'])
                    !!}
                    {!! Form::hidden('in_out_flag', null, ['class' => 'form-control','id' => 'in_out_flag'])
                    !!}
                    <div class="form-group{{ $errors->has('checkinoutdatetime') ? ' has-error' : ''}}">
                        {!! Form::label('checkinoutdatetime', trans('job.checkinoutdatetime'), ['class' => 'col-md-4 control-label required']) !!}
                        <div class="col-md-6">
                            {!! Form::text('checkinoutdatetime', null, ['class' => 'form-control', 'id' => 'checkinoutdatetime']) !!}
                            <p style="color:red" class="help-block checkinoutdatetimeerr"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success checkInOutTools">Save</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('script-head')
<script>
$('.checkinoutdatetimeerr').html('');
    $('#checkinoutdatetime').datetimepicker({
        format: 'YYYY-MM-DD hh:mm:ss',

    });
var url ="{{ url('/admin/tools/') }}";
var img_path ="{{ url('tools/') }}";

        datatable = $('#tools-table').DataTable({
          //  "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('toolControllertoolsData') !!}',
                    type: "get", // method , by default get

                },
                columns: [
               // { data: 'id', name: 'id',"searchable": false },
                   {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

							return o.name;

                        }
			        } ,
                     {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

							var img=o.tool_image;
							if(img){
								return '<a href="'+o.tool_image+'" target="_blank" ><img src="'+o.tool_image+'" class="product_thumb"></a>';
                            }else{
								return 'No Image';
							}
                        }
			        },
                    { data: 'description',name:'description',"searchable" : false},
                     {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                           if(o.status == 'inactive')

                             status = "<a href='"+url+"/"+o.id+"?status=inactive' data-id="+o.id+" title='active'><button class='btn btn-danger danger btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('tool.inactive')</button></a>";
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('tool.active')</button></a>";

                            return status;

                        }

                    },
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e=""; var d=""; var intools=''; var outtools='';
                                // /"+url+"/"+o.id+"/0/checkInOutTools
                                if(o.flag_check_in_out == 1)
                                intools= "<a href='#' data-inout='0' data-id="+o.id+" class='checkInOutToolsView'><button class='btn btn-success btn-xs' type='button' data-toggle='modal'  data-target='#checkInOutToolsView' >Check In</button></a>&nbsp;";

                                outtools= "<a href='#' data-inout='1' class='checkInOutToolsView' data-id="+o.id+"><button class='btn btn-success btn-xs' type='button' data-toggle='modal'  data-target='#checkInOutToolsView'>Check Out </button></a>&nbsp;";

                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";

                                var l = "<a href='"+url+"/logs/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-list' aria-hidden='true'></i> @lang('material.logs')</button></a>&nbsp;";
                            //var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('document.view')</button></a>&nbsp;";


                            return e+d+l+intools+outtools;
                        }
                    }

                ]
        });

        $(document).on('click', '.checkInOutToolsView', function (e) {
        var tool_id = $(this).attr('data-id');
        var in_out_flag = $(this).attr('data-inout');
        $('#tool_id').val(tool_id);
        $('#in_out_flag').val(in_out_flag);
    });
    $(document).on('click', '.checkInOutTools', function (e) {
        var datetime = $('#checkinoutdatetime').val();
        if(datetime != '' && datetime != null)
        {
            $('#checkInOutToolsView').modal('hide');
            $('.checkinoutdatetimeerr').html('');
            var tool_id = $('#tool_id').val();
            var in_out_flag = $('#in_out_flag').val();
            var url ="{{ url('/admin/tools/checkInOutTools') }}";

            var data = {'datetime' : datetime}
            $.ajax({
                type: "post",
                url: url ,
                data : {'date_time' : datetime,'tool_id':tool_id,'in_out_flag':in_out_flag},
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    if(data.code == 400)
                    {
                        toastr.error('Action Not Procede!', data.message);
                    }
                    else
                    {
                        toastr.success('Action Success!', data.message);
                    }

                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        else
        {
            $('.checkinoutdatetimeerr').html('Please Select Date Time.')
        }

    });
     $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/tools/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete tool ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });




</script>
@endpush
