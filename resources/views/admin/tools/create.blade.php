@extends('layouts.backend')


@section('title',trans('tool.add_tool'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('tool.add_new_tool')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('tool.add_new_tool')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('tool.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/tools', 'class' => 'form-horizontal','id'=>'formtool','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.tools.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

