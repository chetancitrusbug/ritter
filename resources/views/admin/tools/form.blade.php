
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', trans('tool.name'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(isset($tool) && $tool->tool_image != null)

<div class="form-group {{ $errors->has('tool_image') ? 'has-error' : ''}}">
    <img src="{!! $tool->tool_image !!}" alt="tool" width="100px">
    {!! Form::label('tool_image', trans('tool.image'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="file" name="tool_image" id="tool_image" multiple> {!! $errors->first('tool_image', '
        <p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
@else
<div class="form-group {{ $errors->has('tool_image') ? 'has-error' : ''}}">
    {!! Form::label('tool_image', trans('tool.image'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="file" name="tool_image" id="tool_image" multiple> {!! $errors->first('tool_image', '
        <p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
@endif

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('tool.description'), ['class' => 'col-md-4 control-label required']) !!}
     <div class="col-md-6">
    {!! Form::textarea('description', null, ['class' => 'form-control','size' => '30x5']) !!}
    {!! $errors->first('description', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', trans('tool.status'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
         {!! Form::select('status',['' =>'Select Status', 'active'=>'Active','inactive'=>'Inactive'] ,null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('tool.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

