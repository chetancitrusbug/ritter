<!DOCTYPE html>
<html>
	<head>
		<title>Ritter</title>
		<style>
			h2 {
				color:#BF1E2E;
				font-size: 20px;
				text-align: center;
			}
			table {
				border-collapse: collapse;
				width: 50%;
				text-align: center;
				margin:0px auto 20px auto;

			}

			th, td {
				text-align: left;
				padding: 8px;
				width: 50%;
				font-size: 15px;
			}

			tr:nth-child(odd){background-color: #f2f2f2}

			th {
				background-color: #4CAF50;
				color: white;
			}

			p {
				margin:30px;
				font-size:15px;
			}

		</style>
	</head>
	<body style="margin:30px auto; width:700px;">

		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
            <h2>CHANGE ORDER FORM JOB (#{{$job->job_number}})</h2>
            <p>I am authorizing this extra work, final price TBD.</p>
            <p>Hello Admin,</p>

            <p>Job Title:<a href="{{ url('/admin/job/'.$job->id.'/edit') }}">{{$job->title}}</a><p>
			<table>

				<tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>

                @foreach ($jobcarddetail as $item)
                <tr>
                    <td>{{$item->job_card_name}}</td>
                    <td>{{$item->job_card_qty}}</td>
                    <td>{{$item->job_card_price}}</td>
                    <td>{{$item->total}}</td>
                </tr>
                @endforeach

			</table>
		</div>

	</body>
</html>