<!DOCTYPE html>
<html>
	<head>
		<title>Ritter</title>
		<style>
			h2 {
				color:#BF1E2E;
				font-size: 20px;
				text-align: center;
			}
			table {
				border-collapse: collapse;
				width: 50%;
				text-align: center;
				margin:0px auto 20px auto;

			}

			th, td {
				text-align: left;
				padding: 8px;
				width: 50%;
				font-size: 15px;
			}

			tr:nth-child(odd){background-color: #f2f2f2}

			th {
				background-color: #4CAF50;
				color: white;
			}

			p {
				margin:30px;
				font-size:15px;
			}

		</style>
	</head>
	<body style="margin:30px auto; width:700px;">

		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
            <p>hello admin,</p>
            <h2>Product Inquiry<h2>
			<table>

				<tr>
                    <td>User Name:</td>
                    <td>{{ $users->name }}</td>
                </tr>
                <tr>
                    <td>Product Name:</td>
                    <td>{{ $productlist->name }}</td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td>{{ $productdata->emailid }}</td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>{{ $productdata->name }}</td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td>{{ $productdata->description }}</td>
                </tr>
                <tr>
                    <td>Quantity:</td>
                    <td>{{ $productdata->quantity }}</td>
                </tr>
			</table>
		</div>

	</body>
</html>