@extends('layouts.backend')


@section('title',trans('video.add_video'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('video.add_new_video')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('video.add_new_video')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('video.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/videos', 'class' => 'form-horizontal','id'=>'formVideo','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.videos.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

