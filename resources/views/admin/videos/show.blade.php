@extends('layouts.backend')

@section('title',trans('video.view_video'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('video.video')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('video.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/videos/' . $video->id . '/edit') }}" title="Edit Video">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('video.edit_video')
                        </button>
                    </a>
                    @endif
                    
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('video.id')</td>
                                <td>{{ $video->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('video.title')</td>
                                <td>{{ $video->title }}</td>
                            </tr>
                       
                            <tr>
                                <td>@lang('video.description')</td>
                                <td>{{ $video->description }}</td>
                            </tr>
                            <tr>
                                <td>Video</td>
                                <td><a href="{{ url('Video') }}/{{ $video->video_url }}" target="_blank">Download here<a></td></a>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection