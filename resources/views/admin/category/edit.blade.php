@extends('layouts.backend')

@section('title',trans('category.edit_category'))
@section('pageTitle',trans('category.edit_category'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('category.edit_category')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('category.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($category, [
                        'method' => 'PATCH',
                        'url' => ['/admin/category', $category->id],
                        'class' => 'form-horizontal',
                        'id'=>'formCategory',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.category.form', ['submitButtonText' => trans('category.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
