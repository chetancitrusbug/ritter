@extends('layouts.backend')


@section('title',trans('product.add_product'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('product.add_new_product')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('product.add_new_product')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('product.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/products', 'class' => 'form-horizontal','id'=>'formProduct','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.products.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

