@extends('layouts.backend')

@section('title',trans('client.edit_client'))
@section('pageTitle',trans('client.edit_client'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('client.edit_client')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('client.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($client, [
                        'method' => 'PATCH',
                        'url' => ['/admin/clients', $client->id],
                        'class' => 'form-horizontal',
                        'id'=>'formClient',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.clients.form', ['submitButtonText' => trans('client.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
