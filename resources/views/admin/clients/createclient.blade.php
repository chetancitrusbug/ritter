@extends('layouts.backend')
@section('title','Clients')


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     Create New Clients
                                                  </div>

                               </div>
                               <div class="box-content ">
                
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('client.back')</button></a>
                        <br />
                        <br />

                       
                              
                            {!! Form::open(['url' => '/admin/clients/createclient', 'class' => 'form-horizontal','method'=>'POST','enctype' => 'multipart/data','files'=>'true','id'=>'Formuploadccsv']) !!}

                                <div class="form-group">
                                    {!! Form::label('file','Upload csv', ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::file('file', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-4">
                                        {!! Form::submit('Upload', ['class' => 'btn btn-primary']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}
                       

                        
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script-head')
<script type="text/javascript">
    $("#Formuploadccsv").validate({

        rules: { 
            file: {
                required: true
            }      
        },
        messages: {
            file: {
                required: "Upload Csv File"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

</script>
@endpush