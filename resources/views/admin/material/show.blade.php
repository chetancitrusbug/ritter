@extends('layouts.backend')

@section('title',trans('material.view_material'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('material.material')</div>
                <div class="panel-body">

                    <a href="{{ url('/admin/material') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('material.back')
                        </button>
                    </a>
                    
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('material.id')</td>
                                <td>{{ $material->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('material.material_name')</td>
                                <td>{{ $material->material_name }}</td>
                            </tr>
                            <tr>
                                <td>@lang('material.min_stock')</td>
                                <td>{{ $material->min_stock }}</td>
                            </tr>
                             <tr>
                                <td>@lang('material.stock')</td>
                                <td>{{ $material->stock }}</td>
                            </tr>
                      
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection