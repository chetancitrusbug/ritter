<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFolderdocumentTable extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('folderdocument', function (Blueprint $table) {
            $table->string('user_type')->nullable();
            $table->string('employee_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('folderdocument', function (Blueprint $table) {
            $table->dropColums('user_type');
            $table->dropColums('employee_id');
        });
    }
}
