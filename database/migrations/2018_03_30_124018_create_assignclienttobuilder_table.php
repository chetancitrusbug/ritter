<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignclienttobuilderTable extends Migration
{
    public $table='assign_client_to_builder';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('assign_client_to_builder', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->nullable();
            $table->integer('client_builder_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_client_to_builder');
    }
}
