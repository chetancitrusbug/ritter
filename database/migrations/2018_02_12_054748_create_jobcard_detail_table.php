<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobcardDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobcarddetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_card_id')->nullable();
            $table->string('job_card_name')->nullable();
            $table->integer('job_card_qty')->nullable();
            $table->float('job_card_price')->nullable();
            $table->float('total')->nullable();
            $table->string('job_card_status')->nullable();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('jobcarddetail');
    }
}
